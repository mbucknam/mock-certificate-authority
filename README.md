
#                            Test Certificate Authority


This is a rudimentary Test Certificate Authority that is useful for quickly creating SSL certificates for large numbers
of servers that need to share trust.  There is one trusted certificate that can be used to enable SSL trust for all
certificates issued by this CA.

The included scripts will automatically create the universal JKS truststore file to be used on all hosts as well as a
pem encoded certificate and key, a JKS keystore file with the host certificate, a PKCS 12 encoded key pair for use in
browsers, and nss databases for services like qpidd.

##                                                Quick Start

Update the file hostlist with the fqdn of hosts that need certificates

DO NOT ADD ANY BLANK LINES OR COMMENT LINES.  ONE HOSTNAME PER LINE

Run the init-ca script

ONLY RUN THIS SCRIPT ONCE.  RUNNING IT AGAIN WILL COMPLETELY RE-INITIALIZE THE CERTIFICATE AUTHORITY DATABASE

    ./init-ca.sh

The only user interaction will be entering a password for the initialization of the nss database.  This should be left
blank unless it is required.

Run the create-certs script

    ./create-certs.sh

All generated files are located in the /pki and nss/ folder.  The pki folder contains files by fqdn.  The nss folder
contains only one nss database with all of the certificates which is not recommended for other than test systems.

For that matter, this whole test certificate authority is not recommended for anything but testing.

It is possible to add additional certificates by creating a NEW hostlist file WITH ONLY THE NEW HOST FQDN's.

REMOVE ALL OF THE OLD FQDN's FROM THE FILE BEFORE RUNNING THE create-certs SCRIPT.


##                                        Testing Certificates

Below is a collection of various commands for testing the certificates and the servers they are running in.

###                                            Testing a server with a client key

    openssl s_client -connect c2e.tcri.navy.mil:8443 -CAfile ca/cacert.pem \
    -cert pki/c2e.tcri.navy.mil.pem -key pki/c2e.tcri.navy.mil.key  -state -debug

###                                    Check the CA certificate (or any other certificate)

    openssl x509 -in ca/cacert.pem -noout -text

###                                        Verifying a certificate is issued by a CA

    openssl verify -verbose -CAfile cacert.pem  server.crt

###                                            Verify Private Key and Certificate Match

(Shamelessly stolen from (and expanding upon) The Apache SSL FAQ)

The private key contains a series of numbers. Two of those numbers form the "public key", the others are part of your
"private key".

The "public key" bits are also embedded in your Certificate (we get them from your CSR). To check that the public key in
your cert matches the public portion of your private key, you need to view the cert and the key and compare the numbers.

To view the Certificate and the key run the commands:

    openssl x509 -noout -text -in server.crt
    openssl rsa -noout -text -in server.key

The modulus and the public exponent portions in the key and the Certificate must match. But since the public exponent is
usually 65537 and it's difficult comparing long moduli you can use the following approach:

    openssl x509 -noout -modulus -in server.crt | openssl md5
    openssl rsa -noout -modulus -in server.key | openssl md5

And then compare these really shorter numbers. With overwhelming probability they will differ if the keys are different.

As a one-liner:

    openssl x509 -noout -modulus -in server.pem | openssl md5 ;\
    openssl rsa -noout -modulus -in server.key | openssl md5

And with auto-magic comparison (If more than one hash is displayed, they don't match):

    (openssl x509 -noout -modulus -in server.pem | openssl md5 ;\
    openssl rsa -noout -modulus -in server.key | openssl md5) | uniq

BTW, if I want to check to which key or certificate a particular CSR belongs you can compute

    openssl req -noout -modulus -in server.csr | openssl md5
