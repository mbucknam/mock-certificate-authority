#!/bin/bash
#
#
# Checks certificates issued by the test openssl configuration
#
#
#

#######################################
# Prints help with script commands
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
show_help() {
    echo
    echo 'Usage: check-certs.sh [options] '
    echo
    echo 'Options:'
    echo '  -v, --verbose               print details of a certificate'
    echo '  -h, --help                  show this help message and exit'
    echo '  -y, --assumeyes             answer yes for all questions'
    echo '  -l, --list                  prints a list of generated certificates'
    echo '  -c [certificate name], --certname=[certificate name]'
    echo '                              certificate name to list (only valid with list)'
    echo '  --caconfig=[openssl ca config file]           
                                        use this file as the openssl config file for generating the CA'
    echo '  --csrconfig=[openssl csr config file]           
                                        use this file as the openssl config file for generating the CSRs'
}


if [ -r "config/setenv.sh" ]; then
    echo "Using config/setenv.sh file"
    . config/setenv.sh
else 
    echo "Using default CA name and config files"
    CA_HOME='ca'
    OPENSSL_CA_CONF='config/mock.ca.cnf'
    OPENSSL_CSR_CONF='config/mock.csr.cnf'

    echo "Using default CSR params"
    CA_DN="Test Certificate Authority"
    ORG_UNIT_1=Test
    ORG_UNIT_2=Certificate
    ORG_UNIT_3=Authority
    ORG="Test Certificates Inc."
    COUNTRY=US

fi

if [ ! -d "$CA_HOME" ]; then
    echo 'You must initialize the Certificate Authority before running this script'
    exit 1
fi

if ! command -v keytool >/dev/null 2>&1; then
        echo ''
        echo 'keytool is required but was not found on the path'
        echo 'Try finding where java is and create a symlink to keytool'
        echo 'example : '
        echo '   ln -s /usr/java/default/bin/keytool /usr/bin/keytool'
        echo ''
        exit 1
fi

#
# Only put one option per argument; all options go before any positional parameters (i.e. non-option arguments); and for
# options with values (e.g. -o above), the value has to go as a separate argument (after a space).
#
# To modify this for other scripts :
#
# Specify short options after -o
# Specific long options after --long
# Put a colon after options that take a value
# Change the program name after -n 
#
TEMP=`getopt -o vhylc: --long verbose,help,assumeyes,list,certname:,caconfig:,csrconfig: \
             -n 'create-certs.sh' -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around `$TEMP': they are essential!
# Must use eval form with gnu getopt
eval set -- "$TEMP"

VERBOSE=false
HELP=false
ASSUME_YES=false
LIST=false
CERT_NAME=

while true; do
  case "$1" in
    -v | --verbose ) VERBOSE=true; shift ;;
    -h | --help ) HELP=true; shift ;;
    -y | --assumeyes ) ASSUME_YES=true; shift ;;
    -l | --list ) LIST=true; shift ;;
    -c | --certname ) CERT_NAME="$2"; shift 2 ;;
    --caconfig ) OPENSSL_CA_CONF="$2"; shift 2 ;;
    --csrconfig ) OPENSSL_CSR_CONF="$2"; shift 2 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if $HELP ; then
    show_help
    exit
fi

echo "VERBOSE       : $VERBOSE"
echo "HELP          : $HELP"
echo "ASSUME_YES    : $ASSUME_YES"
echo "LIST          : $LIST"
echo "CERT_NAME     : $CERT_NAME"

if $LIST ; then
    echo
    echo "PEM : " 
    echo
    if [ -z "$CERT_NAME" ]; then
        for i in $(cd pki;ls *.pem)
        do
            if $VERBOSE ; then
                openssl x509 -in pki/$i -noout -text
            else
                echo "  $i"
            fi
        done
    else    
        openssl x509 -in pki/$CERT_NAME.pem -noout -text
    fi

    echo
    echo "NSS database : "
    if [ -z "$CERT_NAME" ]; then
        certutil -L -d nss        
    else    
        certutil -L -n "$CERT_NAME" -d nss
    fi
    
    echo
    exit 
fi

if [ -z "$CERT_NAME" ]; then
    show_help
    exit
else
    openssl x509 -in pki/$CERT_NAME.pem -noout -text
fi





