#!/bin/bash
#
#
# Initializes a test certificate authority using a test openssl configuration
#
#
#

CONFIG_FILE="config/setenv.sh"

#######################################
# Prints help with script commands
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
show_help() {
    echo
    echo 'Usage: init-ca.sh [options] '
    echo
    echo 'Options:'
    echo '  -h, --help                  show this help message and exit'
    echo '  -y, --assumeyes             answer yes for all questions'
    echo '  --caconfig=[openssl ca config file]
                                        use this file as the openssl config file for generating the CA'
}

pre_check() {

    if [ ! -f "$CONFIG_FILE" ]; then
        echo 
        echo "Unable to find CONFIG_FILE : $CONFIG_FILE"
        echo
        exit 1
    fi

    if [ ! -r "$CONFIG_FILE" ]; then
        echo 
        echo "Unable to read CONFIG_FILE : $CONFIG_FILE"
        echo
        exit 1
    fi

    echo "Using CONFIG_FILE : $CONFIG_FILE"

    if ! command -v keytool >/dev/null 2>&1; then
            echo ''
            echo 'keytool is required but was not found on the path'
            echo 'Try finding where java is and create a symlink to keytool'
            echo 'example : '
            echo '   ln -s /usr/java/default/bin/keytool /usr/bin/keytool'
            echo ''
            exit 1
    fi    
}

configure_environment() {

    . $CONFIG_FILE

    #
    # Only put one option per argument; all options go before any positional parameters (i.e. non-option arguments); and for
    # options with values (e.g. -o above), the value has to go as a separate argument (after a space).
    #
    # To modify this for other scripts :
    #
    # Specify short options after -o
    # Specific long options after --long
    # Put a colon after options that take a value
    # Change the program name after -n 
    #
    TEMP=`getopt -o hy: --long help,assumeyes,caconfig: \
                 -n 'init-ca.sh' -- "$@"`

    if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

    # Note the quotes around `$TEMP': they are essential!
    # Must use eval form with gnu getopt
    eval set -- "$TEMP"

    HELP=false
    ASSUME_YES=false
    while true; do
      case "$1" in
        -h | --help ) HELP=true; shift ;;
        -y | --assumeyes ) ASSUME_YES=true; shift ;;
        --caconfig ) OPENSSL_CA_CONF="$2"; shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
      esac
    done

    if $HELP ; then
        show_help
        exit
    fi   

    echo "HELP              : $HELP"
    echo "ASSUME_YES        : $ASSUME_YES"
    echo "OPENSSL_CA_CONF   : $OPENSSL_CA_CONF"
    echo
    echo "Using Certificate Authority DN (formatted for openssl) :"
    echo
    echo "  $REQ_CA_DN"        
    echo 
    
    DN="$REQ_CA_DN"
}

validate_environment() {

    echo "Validating environment"

    if [ -r "$CA_HOME/private/cakey.pem" ]; then
        echo
        echo "It appears the Certificate Authority has already been initialized"
        echo
        echo "Running this command again will COMPLETELY DELETE AND RECREATE THE CA"
        echo "AND ALL ASSOCIATED FILES" 
        echo

        read -r -p "Are you sure? [y/N] " response
        if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
        then
            echo "Continuing..."
        else
            echo "Exiting..."
            exit 1
        fi
    fi

    if [ ! -f "$OPENSSL_CA_CONF" ]; then
        echo 
        echo "Unable to find OPENSSL_CA_CONF : $OPENSSL_CA_CONF"
        echo
        exit 1
    fi

    if [ ! -r "$OPENSSL_CA_CONF" ]; then
        echo 
        echo "Unable to read OPENSSL_CA_CONF : $OPENSSL_CA_CONF"
        echo
        exit 1
    fi

}


initialize() {

    echo "Initializing"

    rm -rf $CA_HOME
    rm -rf pki/*
    rm -rf csr/*
    rm -rf nss/*

    mkdir $CA_HOME
    mkdir $CA_HOME/certs
    mkdir $CA_HOME/crl
    mkdir $CA_HOME/newcerts
    mkdir $CA_HOME/private
    echo 01 > $CA_HOME/serial
    echo 01 > $CA_HOME/crlnumber

    mkdir -p pki
    mkdir -p csr
    mkdir -p nss

    touch pki/cert-data
    touch $CA_HOME/index.txt

}



generate_ca() {

    echo "Generating CA certificate and key"
    openssl req -new -newkey rsa:4096 -sha256 -x509 -days 1200 -batch -nodes -extensions v3_ca -subj "$DN" -out $CA_HOME/cacert.pem -keyout $CA_HOME/private/cakey.pem -config ./$OPENSSL_CA_CONF

}

generate_jks() {

    echo "Creating a new empty JKS and importing the CA certificate with alias : $CN"
    keytool -importcert -file $CA_HOME/cacert.pem -alias "$CN" -keystore pki/truststore.jks -storepass password -noprompt
}

generate_nss() {

    echo "Creating new empty NSS database with empty password"
    certutil --empty-password -N -d nss

}

load_nss_cert() {

    echo "Importing certificate authority trusted certificate into NSS database with alias : $CN"
    certutil -A -d nss -n "$CN" -t "CT,," -a -i $CA_HOME/cacert.pem

}

main() {

    pre_check
    configure_environment
    validate_environment
    initialize

    generate_ca
    generate_jks
    generate_nss
    load_nss_cert
}

main

echo "All finished.  Ready to run create-certs script"
exit 0

