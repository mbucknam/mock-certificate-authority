#!/bin/bash
#
# Configures the CA
#
#
#

DOMAIN=example.com

# CA home and config file (relative to install directory)
CA_HOME='ca'
OPENSSL_CA_CONF='config/mock.ca.cnf'
OPENSSL_CSR_CONF='config/mock.csr.cnf'

# Components of the certificate that should not need to be changed
CA_CN="Mock Certificate Authority"
OU_0='EXAMPLE'
OU_1='Mock'
OU_2='Test'
ORG='EXAMPLE'
COUNTRY='US'

CA_DN_ARRAY=("CN=$CA_CN" "OU=$OU_1" "OU=$OU_2" "O=$ORG" "C=$COUNTRY")
for (( idx=${#CA_DN_ARRAY[@]}-1 ; idx>=0 ; idx-- )) ; do
    rdn="${CA_DN_ARRAY[idx]}"
    REQ_CA_DN=$REQ_CA_DN'/'$rdn
done

CERT_RDN_ARRAY=( "OU=$OU_0" "OU=$OU_1" "OU=$OU_2" "O=$ORG" "C=$COUNTRY")
for (( idx=${#CERT_RDN_ARRAY[@]}-1 ; idx>=0 ; idx-- )) ; do
    rdn="${CERT_RDN_ARRAY[idx]}"
    REQ_CERT_RDN=$REQ_CERT_RDN'/'$rdn
done

