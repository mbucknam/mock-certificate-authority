#!/bin/bash
#
#
# Creates certificates using a test openssl configuration
#
#
#

CONFIG_FILE="config/setenv.sh"

#######################################
# Prints help with script commands
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
show_help() {
    echo
    echo 'Usage: create-certs.sh [options] '
    echo
    echo 'Options:'
    echo '  -h, --help                  show this help message and exit'
    echo '  -u, --updateonly            create new certificates only'
    echo '  -y, --assumeyes             answer yes for all questions'
    echo '  --cn=[common name]          use this as the common name (if unqualified hostname, domain will be appended)'
    echo '  --domain=[domain]           use this as the domain name (default uses current unless hostfile contains fqdn)'
    echo '  --caconfig=[openssl ca config file]           
                                        use this file as the openssl config file for generating the CA'
    echo '  --csrconfig=[openssl csr config file]           
                                        use this file as the openssl config file for generating the CSRs'
}

pre_check() {

    if [ ! -f "$CONFIG_FILE" ]; then
        echo 
        echo "Unable to find CONFIG_FILE : $CONFIG_FILE"
        echo
        exit 1
    fi

    if [ ! -r "$CONFIG_FILE" ]; then
        echo 
        echo "Unable to read CONFIG_FILE : $CONFIG_FILE"
        echo
        exit 1
    fi

    echo "Using CONFIG_FILE : $CONFIG_FILE"

    if ! command -v keytool >/dev/null 2>&1; then
            echo ''
            echo 'keytool is required but was not found on the path'
            echo 'Try finding where java is and create a symlink to keytool'
            echo 'example : '
            echo '   ln -s /usr/java/default/bin/keytool /usr/bin/keytool'
            echo ''
            exit 1
    fi    
}

configure_environment() {

    . $CONFIG_FILE

    #
    # Only put one option per argument; all options go before any positional parameters (i.e. non-option arguments); and for
    # options with values (e.g. -o above), the value has to go as a separate argument (after a space).
    #
    # To modify this for other scripts :
    #
    # Specify short options after -o
    # Specific long options after --long
    # Put a colon after options that take a value
    # Change the program name after -n 
    #
    TEMP=`getopt -o huy: --long help,updateonly,assumeyes,domain:,cn:,caconfig:,csrconfig:,minheap:,maxheap: \
                 -n 'create-certs.sh' -- "$@"`

    if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

    # Note the quotes around `$TEMP': they are essential!
    # Must use eval form with gnu getopt
    eval set -- "$TEMP"

    HELP=false
    UPDATE_ONLY=false
    ASSUME_YES=false
    DOMAIN=
    JAVA_MISC_OPT=
    while true; do
      case "$1" in
        -h | --help ) HELP=true; shift ;;
        -u | --updateonly ) UPDATE_ONLY=true; shift ;;
        -y | --assumeyes ) ASSUME_YES=true; shift ;;
        --domain ) DOMAIN="$2"; shift 2 ;;
        --cn ) CN="$2"; shift 2 ;;
        --caconfig ) OPENSSL_CA_CONF="$2"; shift 2 ;;
        --csrconfig ) OPENSSL_CSR_CONF="$2"; shift 2 ;;
        --minheap )
          JAVA_MISC_OPT="$JAVA_MISC_OPT -XX:MinHeapFreeRatio=$2"; shift 2 ;;
        --maxheap )
          JAVA_MISC_OPT="$JAVA_MISC_OPT -XX:MaxHeapFreeRatio=$2"; shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
      esac
    done

    if $HELP ; then
        show_help
        exit
    fi   

    if [ -z "$DOMAIN" ]; then

        # If hostname -f returns the fqdn this can be used.  
        # Split domain from fqdn
        fqdn=$(hostname -f)
        DOMAIN=$(echo $fqdn |awk -F. '{$1="";OFS="." ; print $0}' | sed 's/^.//' | sed 's/ /./')
        if [ -z "$DOMAIN" ]; then
            echo "Unable to identify domain using command : 'hostname -f'"     

            # The 'domainname command works on RHEL/Centos 6/7 by setting it in /etc/sysctl.conf :
            #   kernel.domainname=tcri.navy.mil
            # May have to reboot to take effect.
            DOMAIN="$(domainname)"
            if [ "$DOMAIN" == "(none)" ]; then
                echo "Unable to identify domain using command : 'domainname'"     
                unset DOMAIN
            fi
        fi
    fi

    echo "HELP              : $HELP"
    echo "UPDATE_ONLY       : $UPDATE_ONLY"
    echo "ASSUME_YES        : $ASSUME_YES"
    echo "DOMAIN            : $DOMAIN"
    echo "CN                : $CN"
    echo "OPENSSL_CA_CONF   : $OPENSSL_CA_CONF"
    echo "OPENSSL_CSR_CONF  : $OPENSSL_CSR_CONF"
    echo
    echo "Using Certificate RDN (formatted for openssl) :"
    echo
    echo "  $REQ_CERT_RDN"        
    echo 
}

validate_environment() {

    echo "Validating environment"

    if [ ! -d "$CA_HOME" ]; then
        echo 'You must initialize the Certificate Authority before running this script'
        exit 1
    fi

    if [ ! -f "$OPENSSL_CA_CONF" ]; then
        echo 
        echo "Unable to find OPENSSL_CA_CONF : $OPENSSL_CA_CONF"
        echo
        exit 1
    fi

    if [ ! -r "$OPENSSL_CA_CONF" ]; then
        echo 
        echo "Unable to read OPENSSL_CA_CONF : $OPENSSL_CA_CONF"
        echo
        exit 1
    fi

    if [ ! -f "$OPENSSL_CSR_CONF" ]; then
        echo 
        echo "Unable to find OPENSSL_CSR_CONF : $OPENSSL_CSR_CONF"
        echo
        exit 1
    fi

    if [ ! -r "$OPENSSL_CSR_CONF" ]; then
        echo 
        echo "Unable to read OPENSSL_CSR_CONF : $OPENSSL_CSR_CONF"
        echo
        exit 1
    fi

    if [ "x$DOMAIN" = "x" ]; then
        echo 
        echo "Unable to identify domain : use --domain=[domain]"
        echo
        exit 1
    fi

}

generate_csr() {

    echo "Generating the CSR"

    DN="$REQ_CERT_RDN/CN=$CN"

    # Use $OPENSSL_CSR_CONF sans
    #openssl req -new -newkey rsa:2048 -sha256 -batch -nodes -subj "$DN" -out "csr/$CN.csr" -keyout "pki/$CN.key" -config $OPENSSL_CSR_CONF
    
    # Pipe sans in
openssl req -new -newkey rsa:2048 -sha256 -batch -nodes -subj "$DN" -out "csr/$CN.csr" -keyout "pki/$CN.key" \
-config <(cat $OPENSSL_CSR_CONF <(printf "[req_sans]\nsubjectAltName=$SANS"))


    if [ ! -r "pki/$CN.key" ]; then
        echo 
        echo "There was a problem generating the private key : $CN.key"
        echo
        exit 1
    fi

    if [ ! -r "csr/$CN.csr" ]; then
        echo 
        echo "There was a problem generating the CSR : $CN.csr"
        echo
        exit 1
    fi

}

sign_csr() {

    echo "Signing the CSR"

    echo y > pki/cert-data
    echo y >> pki/cert-data
    
    openssl ca -config ./$OPENSSL_CA_CONF -policy policy_anything -out pki/$CN.pem -days 1200  -extensions v3_req -infiles csr/$CN.csr < pki/cert-data

    if [ ! -r "pki/$CN.pem" ]; then
        echo 
        echo "There was a problem signing the CSR : $CN.csr"
        echo
        exit 1
    fi

}

generate_pkcs12_cert() {

    openssl pkcs12 -export -in pki/$CN.pem -inkey pki/$CN.key -out pki/$CN.p12 -passout pass:password

    if [ ! -r "pki/$CN.p12" ]; then
        echo 
        echo "There was a problem generating the pkcs12 certificate : $CN.p12"
        echo
        exit 1
    fi
}

generate_jks() {

    # Create a new empty JKS and import the PKCS 12 certificate
    keytool -importkeystore -srckeystore pki/$CN.p12 -destkeystore pki/$CN.jks -srcstoretype pkcs12 -deststoretype jks -srcstorepass password -deststorepass password -noprompt

    if [ ! -r "pki/$CN.jks" ]; then
        echo 
        echo "There was a problem generating the Java Key Store (JKS) : $CN.jks"
        echo
        exit 1
    fi

}

load_nss_pkcs12_cert() {

    # Import trusted cert into the nss database
    certutil -A -d nss -n "$CN" -t "CT,," -a -i pki/$CN.pem

    # Import key certificate for trusted cert (trusted cert MUST be imported first for alias to be set)
    pk12util -i pki/$CN.p12 -d nss -W password

}

cleanup() {

    # Clean up 
    rm pki/cert-data

}

set_file_permissions() {

    chmod 664 nss/*
    chmod 775 nss
    chmod 664 pki/*
    chmod 775 pki

}

process_cn() {

        if [[ $CN != 'localhost' &&  $CN != *"."* ]]
        then
          echo "Adding $DOMAIN to unqualified hostname";
          CN="$CN.$DOMAIN"
        else
          echo "Using fully qualified hostname : $CN";
        fi


        if [ -f "pki/$CN.pem" ]; then
            echo
            echo "WARNING : $CN already has a certificate"       
            if $UPDATE_ONLY ; then
                continue
            elif $ASSUME_YES ; then
                echo
                echo "Regenerating certificate for $CN"
            else
                echo
                read -r -p "Are you sure you want to recreate it? [y/N] " response
                case $response in
                    [yY][eE][sS]|[yY]) 
                        echo
                        echo "-----------------------------------------------------------------------------------"
                        echo
                        echo "Regenerating certificate for $CN"
                        echo
                        echo "-----------------------------------------------------------------------------------"
                        ;;
                    *)    
                        echo "-----------------------------------------------------------------------------------"
                        echo
                        echo "Skipping $CN"
                        echo
                        echo "-----------------------------------------------------------------------------------"    
                        continue
                        ;;
                esac
            fi
        else 
            echo
            echo "-----------------------------------------------------------------------------------"
            echo
            echo "Generating certificate for $CN"
            echo
            echo "-----------------------------------------------------------------------------------"
        fi

        generate_csr
        sign_csr
        generate_pkcs12_cert
        generate_jks
        load_nss_pkcs12_cert
        cleanup
}

process_hosts_sans() {

    
    # Loop through all the entries in the file hostlist
    for cn in $(cat config/hostlist | grep -v '#')
    do
        if [[ $cn != 'localhost' &&  $cn != *"."* ]]
        then
            cn="$cn.$DOMAIN"
        fi

        if [ "x$SANS" = "x" ]; then
            SANS="DNS:$cn"
        else
            SANS="$SANS,DNS:$cn"
        fi

    done

    echo $SANS



}

process_hosts_certs() {

    # Loop through all the entries in the file hostlist
    for i in $(cat config/hostlist | grep -v '#')
    do

        echo
        echo "###################################################################################"
        echo
        echo "Processing hostlist entry : $i"
        echo
        echo "###################################################################################"
        CN="$i"
        process_cn
    done

}

main() {

    pre_check
    configure_environment $args
    validate_environment
    process_hosts_sans
    if [ "x$CN" = "x" ]; then
        echo 
        echo "Creating certificates for all entries in host file"
        echo
        process_hosts_certs
    else
        echo 
        echo "Creating certificates for CN : $CN"
        echo
        process_cn    
    fi

    set_file_permissions
}

args=$@
echo "args : $args"

main 

echo "All finished."
exit 0

